set nocompatible

call plug#begin()
Plug 'tpope/vim-sensible'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-unimpaired'
Plug 'tpope/vim-surround'
Plug 'hynek/vim-python-pep8-indent'
Plug 'ervandew/supertab'
Plug 'scrooloose/syntastic'
Plug 'christoomey/vim-tmux-navigator'
Plug 'kana/vim-textobj-user'
Plug 'kana/vim-textobj-lastpat'
Plug 'mileszs/ack.vim'
call plug#end()

" Tabs
set shiftwidth=4
set tabstop=4
set expandtab
" Clipboard
set mouse=a                     " mouse support in terminals
set clipboard=unnamed           " yanks and cuts go in system clipboard
" Search
set ignorecase
set smartcase
set showmatch
set hlsearch
" Keys
set pastetoggle=<Leader>tp
nnoremap <leader><space> :noh<cr>
" h and l and ~ wrap over lines
set whichwrap=h,l,~
" Q reformats current paragraph or selected text
nnoremap Q gqap
vnoremap Q gq
" Make Y yank to the end of the line (like C and D) rather than the entire
" line
noremap Y y$
" Use up and down to move by screen line
map <up> gk
map <down> gj
vmap <up> gk
vmap <down> gj
inoremap <up> <c-o>gk
inoremap <down> <c-o>gj
nmap k gk
nmap j gj
vmap k gk
vmap j gj

" Show list of possible files on tab completion, rather than first guess
set wildmode=longest,list

set statusline=%f\ %y\ %=%c,%l/%L
if v:version >= 703
    set colorcolumn=80
    set cursorline
endif
if &t_Co > 2 || has("gui_running")
    syntax on
    colorscheme Tomorrow-Night
endif
" Set fullscreen background to same color as normal test
if has("gui_running")
    set gfn=Source\ Code\ Pro:h13
    set fuoptions=maxvert
    set guioptions-=T
endif

" Change default position of new splits
set splitbelow
set splitright

" Open in TeXShop
nnoremap <leader>tx :!open -a TeXShop %<cr><cr>

" Use :w!! to save root files you forgot to open with sudo
ca w!! w !sudo tee "%"

" Syntastic options
let g:syntastic_check_on_open=1
let g:syntastic_python_checkers=["flake8"]

" Format-specific formating
autocmd FileType markdown,text setlocal ai fo+=na comments=n:> tw=72 nojoinspaces spell spelllang=en
autocmd FileType markdown,text setlocal formatlistpat=^\\s\\s*\\d\\+\\.\\s\\+\\\|^\\s*[-*+]\\s\\+\\\|^\\s*\\[.\\]\\s\\+
autocmd FileType tex setlocal tw=72 autoindent nojoinspaces spell spelllang=en
autocmd FileType python setlocal textwidth=79

" Go to the last cursor location when file opened, unless a git commit
au BufReadPost *
    \ if line("'\"") > 0 && line("'\"") <= line("$") && &filetype != "gitcommit" |
        \ execute("normal `\"") |
    \ endif

" Expand %% to directory of file in current buffer (also %:h<Tab>)
cnoremap <expr> %% getcmdtype() == ':' ? expand('%:h').'/' : '%%'

" Learn not to use arrow keys
noremap <Up> <Nop>
noremap <Down> <Nop>
noremap <Left> <Nop>
noremap <Right> <Nop>
inoremap <Up> <Nop>
inoremap <Down> <Nop>
inoremap <Left> <Nop>
inoremap <Right> <Nop>
